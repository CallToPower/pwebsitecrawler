#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the PWebsiteCrawler.
#

"""PWebsiteCrawler - WebsiteParser"""

from html.parser import HTMLParser
from urllib.request import urlopen
from urllib import parse


class WebsiteParser(HTMLParser):
    """Website parser, HTML parser extension"""

    def handle_starttag(self, tag, attrs):
        """Handles tags"""
        if tag == 'a':
            for (key, value) in attrs:
                if key == 'href':
                    if self._is_currentsite(value) and self._is_html(value):
                        new_url = parse.urljoin(self.base_url, value)
                        is_self = new_url == self.base_url
                        is_to_be_visited = new_url in self.links_html
                        is_visited = new_url in self.exclude_pages
                        if not is_self and not is_visited and not is_to_be_visited:
                            self.links_html.add(new_url)
        elif tag in ['link', 'img', 'script']:
            for (key, value) in attrs:
                if key in ['href', 'src']:
                    if self._is_currentsite(value):
                        new_url = parse.urljoin(self.base_url, value)
                        is_to_be_visited = new_url in self.links_files
                        is_visited = new_url in self.exclude_files
                        if not is_to_be_visited and not is_visited:
                            self.links_files.add(new_url)

    def get_data(self, url, exclude_pages=set(), exclude_files=set()):
        """External call"""
        self.links_html = set()
        self.links_files = set()
        self.base_url = url
        self.exclude_pages = exclude_pages
        self.exclude_files = exclude_files
        response = urlopen(url)
        if response.getheader('Content-Type') == 'text/html':
            html_bytes = response.read()
            html_string = html_bytes.decode('utf-8')
            self.feed(html_string)
            return html_string, self.links_html, self.links_files
        else:
            return '', self.links_html, self.links_files

    def _is_currentsite(self, url):
        """Checks whether URL is current base URL"""
        return url.startswith(self.base_url) or not url.startswith('http')

    def _is_html(self, url):
        """Checks whether URL is an HTML document"""
        return url.endswith('/') or url.endswith('html')
