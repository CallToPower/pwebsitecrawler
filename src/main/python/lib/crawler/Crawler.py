#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the PWebsiteCrawler.
#

"""PWebsiteCrawler - Crawler"""

import abc


class Crawler(object, metaclass=abc.ABCMeta):
    """The crawler"""

    @abc.abstractmethod
    def crawl(self):
        raise NotImplementedError('Sub classes of Crawl must define "crawl" method')
