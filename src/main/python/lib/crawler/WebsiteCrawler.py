#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the PWebsiteCrawler.
#

"""PWebsiteCrawler - WebsiteCrawler"""

import urllib.request
import logging
import os
import shutil
from pathlib import Path
import re
from urllib import parse

from lib.crawler.Crawler import Crawler
from lib.crawler.WebsiteParser import WebsiteParser
from lib.Settings import MAX_FILES_CRAWLED


class WebsiteCrawler(Crawler):
    """Website crawler implementation"""

    def __init__(self, url, outputfolder):
        """Initialization"""
        self.url = url
        self.outputfolder = outputfolder if outputfolder else './out'
        self.basedir = None
        self.parser = WebsiteParser()

        self.prepare_outputfolder()

    def prepare_outputfolder(self):
        """Prepares the output folder"""
        logging.info('Preparing output folder \'' + self.outputfolder + '\'')
        self.basedir = os.path.dirname(self.outputfolder)
        if os.path.exists(self.basedir):
            logging.debug('Clearing output folder \'' + self.basedir + '\'')
            try:
                shutil.rmtree(self.basedir)
            except Exception as e:
                logging.error(e)
        os.makedirs(self.basedir)

    def crawl(self):
        """Crawls the site"""
        pages_to_visit = set([self.url])
        pages_visited = set()
        files_to_download = set()
        max_pages = MAX_FILES_CRAWLED

        while pages_to_visit and max_pages > 0:
            max_pages -= 1
            url = pages_to_visit.pop()
            pages_visited.add(url)
            logging.info('Analyzing URL {}'.format(url))

            try:
                data, links_html, links_files = self.parser.get_data(
                    url, exclude_pages=pages_to_visit | pages_visited, exclude_files=files_to_download)
                pages_to_visit = pages_to_visit | links_html
                files_to_download.update(links_files)
                self._save_html(url, data)
            except Exception as e:
                logging.error('Exception for URL "{}": {}'.format(url, e))

        logging.info('Pages crawled: {}'.format(pages_visited))

        logging.info('Downloading {} files'.format(len(files_to_download)))
        images_from_css = set()
        for file_url in files_to_download:
            name = self._get_filename(file_url)
            logging.info('Downloading file {}'.format(name))
            filepath = os.path.join(self.basedir, name)
            self._create_file(filepath)
            try:
                urllib.request.urlretrieve(file_url, filepath)
                if name.endswith('.css'):
                    logging.info('Extracting image URLs from CSS')
                    with open(filepath, 'r') as infile:
                        content = infile.read()
                        images = re.findall('url\(([^)]+)\)', content)
                        for img in images:
                            images_from_css.add(img)
            except Exception as e:
                logging.error(
                    'Could not download from URL "{}": {}'.format(file_url, e))

        logging.info('Downloading {} files'.format(len(images_from_css)))
        for file_url in images_from_css:
            file_url = file_url if file_url.startswith(self.url) else parse.urljoin(self.url, file_url)
            name = self._get_filename(file_url)
            logging.info('Downloading file {}'.format(name))
            filepath = os.path.join(self.basedir, name)
            self._create_file(filepath)
            try:
                urllib.request.urlretrieve(file_url, filepath)
            except Exception as e:
                logging.error(
                    'Could not download from URL "{}": {}'.format(file_url, e))

    def _get_filename(self, url):
        """Extracts the correct file name"""
        name = url[len(self.url):]
        if name.startswith('/'):
            name = name[1:]
        elif name.startswith('../'):
            name = name[3:]
        if name.endswith('style.php'):
            name = name[:-len('php')] + 'css'
        return name

    def _create_file(self, filename):
        """Creates a file"""
        if filename:
            basedir = os.path.dirname(filename)
            if not os.path.exists(basedir):
                os.makedirs(basedir)

    def _save_html(self, url, data, default_suffix='html', index_name='index'):
        """Saves data to an HTML file"""
        name = self._get_filename(url)
        name = name + 'index' if name.endswith('/') else name
        filename = os.path.join(
            self.basedir, name + '.' + default_suffix if name else index_name + '.' + default_suffix)
        self._create_file(filename)
        logging.info('Writing content to file \'{}\''.format(filename))
        with open(filename, 'w') as outfile:
            outfile.write(data.replace('style.php', 'style.css'))
