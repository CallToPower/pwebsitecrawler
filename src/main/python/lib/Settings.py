#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the PWebsiteCrawler.
#

"""PWebsiteCrawler - Settings"""

URL_TO_CRAWL = 'https://www.axel-m.de'
OUTPUT_FOLDER = './out/'
MAX_FILES_CRAWLED = 1000
