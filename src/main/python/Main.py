#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the PWebsiteCrawler.
#

"""PWebsiteCrawler - Main"""

import os
import time
import logging
from pathlib import Path

from lib.Settings import URL_TO_CRAWL, OUTPUT_FOLDER
from lib.crawler.WebsiteCrawler import WebsiteCrawler

LOG_TO_FILE = False

# Logging configuration
logging_loglevel = logging.DEBUG
logging_datefmt = '%d-%m-%Y %H:%M:%S'
logging_format = '[%(asctime)s] [%(levelname)-5s] [%(module)-20s:%(lineno)-4s] %(message)s'
logging_logfile = str(Path.home()) + '/logs/coyo.user-importer.application-' + \
    time.strftime('%d-%m-%Y-%H-%M-%S') + '.log'


def _initialize_logger(logtofile=False):
    """Initializes the logging utility"""
    logging.basicConfig(level=logging_loglevel,
                        format=logging_format,
                        datefmt=logging_datefmt)

    if logtofile:
        basedir = os.path.dirname(logging_logfile)
        if not os.path.exists(basedir):
            os.makedirs(basedir)
        handler_file = logging.FileHandler(logging_logfile, mode='w', encoding=None, delay=False)
        handler_file.setLevel(logging_loglevel)
        handler_file.setFormatter(logging.Formatter(
            fmt=logging_format, datefmt=logging_datefmt))
        logging.getLogger().addHandler(handler_file)


if __name__ == '__main__':
    _initialize_logger(logtofile=LOG_TO_FILE)

    logging.info('Crawling URL "{}"'.format(URL_TO_CRAWL))
    crawler = WebsiteCrawler(URL_TO_CRAWL, OUTPUT_FOLDER)
    crawler.crawl()
