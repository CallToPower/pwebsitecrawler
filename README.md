# Python Website Crawler

## Prerequisites

* Python 3

## Usage

* Start shell
  * Windows
    * Start shell as administrator
    * `Set-ExecutionPolicy Unrestricted -Force`
* Create a virtual environment
  * `python -m venv venv`
* Activate the virtual environment
  * Mac/Linux
    * `source venv/bin/activate`
  * Windows
    * `.\venv\scripts\activate`
* Install the required libraries
  * `pip install -r requirements.txt`
* Run the app
  * `python -m fbs run`

## Crawled website

* Start a webserver (e.g. locally via MAMP)
* Copy/Move the files of the output folder (default "./out") to the htdocs folder
